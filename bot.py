#!/usr/bin/env python
import threading
import requests
import random
from json import loads, load
from os.path import join


class ApiError(Exception):
    pass


def unique_bot_id():
    id = random.getrandbits(32)
    while True:
        yield id
        id += 1


class UserBot(threading.Thread):

    def get_auth_headers(self):
        return {'Authorization': f'Bearer {self.token}'}

    def signup(self):
        response = requests.post(
            'http://127.0.0.1:8000/api/signup/', data=self.user_data
        )
        print(f'Sign up user {self.user_data["username"]}')
        return response.status_code == 201

    def get_token(self):
        response = requests.post(
            'http://127.0.0.1:8000/api/token/', data=self.user_data
        )
        print(f'Get token for user {self.user_data["username"]}')
        return loads(response.content)['access'], response.status_code == 200

    def generate_posts(self, posts_count):
        for i in range(posts_count):
            post_data = {
                "title": f'test post {i} by {self.user_data["username"]}',
                "text": f'test text post {i}',
            }
            response = requests.post(
                'http://127.0.0.1:8000/api/post/', data=post_data,
                headers=self.get_auth_headers()
            )
            if not response.status_code == 201:
                return False
        print(f'Generate posts by user {self.user_data["username"]}')
        return True

    def __init__(self, user_id, posts_count, likes_count):
        self.user_data = {
            'username': f'User{user_id}',
            'password': 'User1234567'
        }

        if not self.signup():
            raise ApiError('Sign up api error')

        self.token, status = self.get_token()
        if not status:
            raise ApiError('Get token api error')

        if not self.generate_posts(posts_count):
            raise ApiError('Generate posts error')

        self.likes_count = likes_count
        threading.Thread.__init__(self, name=f'bot {user_id}')

    def run(self):

        response = requests.get(
            'http://127.0.0.1:8000/api/post/',
        )

        posts = loads(response.content)['data']

        for i in range(self.likes_count):
            post = random.choice(posts)
            response = requests.post(
                f'http://127.0.0.1:8000/api/post_like/{post.get("id")}/',
                headers=self.get_auth_headers()
            )
        print(f'User {self.user_data["username"]} finished liking posts')


def run_bot():
    with open(join('conf', 'bot.conf')) as config_file:
        conf = load(config_file)
    id_generator = unique_bot_id()
    users = []
    for i in range(conf.get('number_of_users', 5)):
        try:
            users.append(UserBot(
                next(id_generator),
                conf.get('max_posts_per_user', 10),
                conf.get('max_likes_per_user', 50)
            ))
        except ApiError as e:
            print(f'Error - {e}')
            return

    for user in users:
        user.start()
        user.join()


if __name__ == "__main__":
    run_bot()
