FROM python:3.6
ENV PYTHONUNBUFFERED 1
ENV ISDOCKER 1
RUN mkdir /config
ADD .meta/packages /config/
RUN pip install -r /config/packages
RUN pip install uwsgi
RUN mkdir logs
RUN mkdir app
RUN mkdir /app/src;
WORKDIR /app/src
EXPOSE 8000