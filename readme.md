social network
===========

Description
------------

**Social network** is test app.

---------------------------------------

Deployment
---------------------------

Clone source code:

    git clone https://boris_mishunin@bitbucket.org/boris_mishunin/social_network.git

Create virtual environment:

    virtualenv --python=python3.6 libs
    source libs/bin/activate

Install packages:

    pip install -r .meta/packages

Run database migrations:

    ./manage.py migrate

Start server:

    ./manage.py runserver


Open in browser http://localhost:8000/

---------------------------------------

Bot
---------------------------

Run test bot:

    ./bot.py

Tests
---------------------------

Run tests:

    ./manage.py test --settings social_network.settings api.tests

Api links
---------------------------

**Users links**:

* Sign up: http://localhost:8000/api/signup/. Method: POST. Params {"username": {username}, "password": {password}}
* Get token: http://localhost:8000/api/token/. Method: POST. Params {"username": {username}, "password": {password}}
* User activity: http://localhost:8000/api/user_activity/. Method: GET.

**Posts links**:

* Post list: http://localhost:8000/api/post/. Method: GET
* Post detail: http://localhost:8000/api/post/{post id}. Method: GET
* Post add: http://localhost:8000/api/post/. Method: POST. Params {"title": {post_title}, "text": {post_text}}

**Likes/dislikes**:

* Post like: http://localhost:8000/api/post_like/{post_id}/. Method: POST.
* Post dislike: http://localhost:8000/api/post_dislike/{post_id}/. Method: POST.
* Analitics: http://localhost:8000/api/analitics/?date_from={date}&date_to={date}. Method: GET.
