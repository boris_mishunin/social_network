from django.middleware.locale import LocaleMiddleware
from api.models import Profile
from datetime import datetime
from rest_framework_simplejwt.authentication import JWTAuthentication
from rest_framework_simplejwt.exceptions import InvalidToken


class RequestMiddleware(LocaleMiddleware):
    def process_request(self, request):
        try:
            user = JWTAuthentication().authenticate(request)
        except InvalidToken:
            user = None
        if user is not None:
            profile, _ = Profile.objects.get_or_create(user=user[0])
            profile.last_request_url = request.path
            profile.last_request_date = datetime.now()
            profile.save()
