from rest_framework import serializers
from django.contrib.auth import get_user_model
from .models import Post, PostsLikeDislike, Profile


class UserSerializer(serializers.ModelSerializer):
    """

        Serializer for user.
    """

    password = serializers.CharField(write_only=True)

    class Meta:
        model = get_user_model()
        fields = ("id", "username", "password")

    def create(self, validated_data):

        user = get_user_model().objects.create(
            username=validated_data['username']
        )
        user.set_password(validated_data['password'])
        user.save()

        return user


class PostSerializer(serializers.ModelSerializer):
    """

        Serializer for posts.
    """

    user = UserSerializer(read_only=True)

    class Meta:
        model = Post
        fields = ('id', 'title', 'text', 'user')

    def create(self, validated_data):
        request = self.context['request']
        validated_data['user'] = request.user
        return Post.objects.create(**validated_data)


class AnaliticsSerialiser(serializers.Serializer):
    """

        Serializer for analitics.
    """
    day = serializers.CharField(required=True)
    likes = serializers.IntegerField(required=True)
    dislikes = serializers.IntegerField(required=True)


class PostsLikeDislikeSerializer(serializers.ModelSerializer):
    """

        Serializer for posts.
    """

    class Meta:
        model = PostsLikeDislike
        fields = '__all__'


class ProfileSerializer(serializers.ModelSerializer):
    """

        Serializer for user profiles.
    """

    class Meta:
        model = Profile
        fields = (
            'user', 'last_login_date',
            'last_request_url', 'last_request_date'
        )
