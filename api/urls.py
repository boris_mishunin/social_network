from django.urls import path
from .views import (
    SignUpUserView, PostViewSet,
    PostsLikeDislikeViewSet, ObtainTokenPairView,
    UserActivityViewSet
)
from rest_framework_simplejwt import views as jwt_views


urlpatterns = [
    path('signup/', SignUpUserView.as_view(), name='user-sign-up'),
    path('token/', ObtainTokenPairView.as_view(), name='token_obtain_pair'),
    path('token/refresh/', jwt_views.TokenRefreshView.as_view(),
         name='token_refresh'),
    path('post/', PostViewSet.as_view({
        'get': 'list',
        'post': 'create'
    }), name='posts'),
    path('post/<int:pk>/', PostViewSet.as_view({
        'get': 'retrieve',
    }), name='post'),
    path('analitics/', PostsLikeDislikeViewSet.as_view({
        'get': 'list',
    }), name='analitics'),
    path('post_like/<int:post_id>/', PostsLikeDislikeViewSet.as_view({
        'post': 'post_like'
    }), name='post_like'),
    path('post_dislike/<int:post_id>/', PostsLikeDislikeViewSet.as_view({
        'post': 'post_dislike'
    }), name='post_dislike'),
    path('user_activity/', UserActivityViewSet.as_view({
        'get': 'list'
    }), name='user_activity'),
]
