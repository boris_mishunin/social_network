from rest_framework import permissions
from rest_framework.generics import CreateAPIView
from django.contrib.auth import get_user_model
from .serializers import (
    UserSerializer, PostSerializer, ProfileSerializer,
    PostsLikeDislikeSerializer, AnaliticsSerialiser
)
from .models import Post, PostsLikeDislike, Profile
from rest_framework.permissions import AllowAny
from rest_framework import viewsets
from rest_framework.response import Response
from django.db.models import Q
from django.db.models import Count
from rest_framework import status
from rest_framework.decorators import action
from datetime import datetime
from rest_framework_simplejwt.views import TokenObtainPairView
from rest_framework_simplejwt.exceptions import TokenError, InvalidToken


class ObtainTokenPairView(TokenObtainPairView):
    permission_classes = (permissions.AllowAny,)

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)

        try:
            serializer.is_valid(raise_exception=True)
            if serializer.user is not None:
                profile, _ = Profile.objects.get_or_create(
                    user=serializer.user
                )
                profile.last_login_date = datetime.now()
                profile.save()
        except TokenError as e:
            raise InvalidToken(e.args[0])

        return Response(serializer.validated_data, status=status.HTTP_200_OK)


class SignUpUserView(CreateAPIView):
    model = get_user_model()
    permission_classes = [permissions.AllowAny]
    serializer_class = UserSerializer


class PostViewSet(viewsets.ModelViewSet):
    """

        This view set return posts.
    """

    serializer_class = PostSerializer
    queryset = Post.objects.all()

    def get_permissions(self):
        if self.action in ('list', 'retrieve'):
            return [AllowAny(), ]
        return super(PostViewSet, self).get_permissions()

    def list(self, request, *args, **kwargs):
        posts = super(PostViewSet, self).list(request, *args, **kwargs)
        return Response({"status": "true", "data": posts.data},
                        status=posts.status_code)

    def retrieve(self, request, *args, **kwargs):
        post = super(PostViewSet, self).retrieve(request, *args, **kwargs)
        return Response({"status": "true", "data": post.data},
                        status=post.status_code)

    def create(self, request, *args, **kwargs):
        post = super(PostViewSet, self).create(request, *args, **kwargs)
        return Response({"status": "true", "data": post.data},
                        status=post.status_code)


class PostsLikeDislikeViewSet(viewsets.ModelViewSet):
    """

        This view set return posts likes dislikes.
    """

    serializer_class = PostsLikeDislikeSerializer
    queryset = PostsLikeDislike.objects.all()

    def get_permissions(self):
        if self.action in ('list',):
            return [AllowAny(), ]
        return super(PostsLikeDislikeViewSet, self).get_permissions()

    def list(self, request, *args, **kwargs):
        date_from = request.query_params.get('date_from')
        date_to = request.query_params.get('date_to')
        queryset = PostsLikeDislike.objects

        if date_from is not None and date_to is not None:
            queryset = queryset.filter(updated_at__range=[date_from, date_to])
        elif date_from is None and date_to is not None:
            queryset = queryset.filter(updated_at__lte=date_to)
        elif date_from is not None and date_to is None:
            queryset = queryset.filter(updated_at__gte=date_from)

        analitics = queryset.extra(
            {'day': "date(updated_at)"}
        ).values('day').annotate(
            likes=Count(
                'pk', filter=Q(like_dislike=PostsLikeDislike.TYPES.LIKE)
            ),
            dislikes=Count(
                'pk', filter=Q(like_dislike=PostsLikeDislike.TYPES.DISLIKE)
            )
        )
        serializer = AnaliticsSerialiser(analitics, many=True)
        return Response({"success": True, "data": serializer.data},
                        status=status.HTTP_200_OK)

    def set_likes(self, request, post_id, like_dislike):
        try:
            post = Post.objects.get(id=post_id)
            likes, _ = PostsLikeDislike.objects.get_or_create(
                post=post, user=request.user
            )
            likes.like_dislike = like_dislike
            likes.updated_at = datetime.now()
            likes.save()
            new_likes_data = PostsLikeDislikeSerializer(likes)
            return Response({"status": "true", "data": new_likes_data.data},
                            status=status.HTTP_201_CREATED)

        except Post.DoesNotExist:
            return Response(
                {
                    "success": False,
                    "data": {"error": [f"Post with id {post_id} not found"]}
                },
                status=status.HTTP_400_BAD_REQUEST
            )

    @action(['post'], detail=False)
    def post_like(self, request, post_id):
        return self.set_likes(request, post_id, 1)

    @action(['post'], detail=False)
    def post_dislike(self, request, post_id):
        return self.set_likes(request, post_id, 0)


class UserActivityViewSet(viewsets.ReadOnlyModelViewSet):
    """

        This view set return user activity.
    """

    serializer_class = ProfileSerializer
    queryset = Profile.objects.all()

    def list(self, request, *args, **kwargs):
        profiles = super(
            UserActivityViewSet, self
        ).list(request, *args, **kwargs)
        return Response({"status": "true", "data": profiles.data},
                        status=profiles.status_code)
