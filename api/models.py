from django.db import models
from django.contrib.auth.models import User
from datetime import datetime


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    last_request_url = models.URLField(
        verbose_name='last request url',
        blank=True,
        null=True
    )
    last_request_date = models.DateTimeField(
        verbose_name='last request date',
        blank=True,
        null=True
    )
    last_login_date = models.DateTimeField(
        verbose_name='last login date',
        blank=True,
        null=True
    )

    class Meta:
        db_table = 'user_profile'
        verbose_name = 'User profile'
        verbose_name_plural = 'User profile'

    def __str__(self):
        return f'Profile of {self.user.username}'


class Post(models.Model):
    """

        This model contains posts info
    """

    user = models.ForeignKey(
        User,
        related_name='posts',
        on_delete=models.DO_NOTHING,
        verbose_name='user'
    )

    title = models.CharField(
        max_length=150,
        verbose_name='title',
        help_text='enter posts title'
    )

    text = models.TextField(
        verbose_name='text',
        help_text='enter posts text'
    )

    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        db_table = 'posts'
        verbose_name = 'Post'
        verbose_name_plural = 'Posts'

    def __str__(self):
        return self.title


class PostsLikeDislike(models.Model):
    """

        This model contains posts likes info
    """

    class TYPES:
        LIKE = 1
        DISLIKE = 0

    LIKES_DISLIKES_CHOICES = (
        (TYPES.LIKE, 'Like'),
        (TYPES.DISLIKE, 'Dislike'),
    )

    user = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        verbose_name='user'
    )

    post = models.ForeignKey(
        Post,
        on_delete=models.CASCADE,
        verbose_name='post',
        help_text='select post'
    )

    like_dislike = models.SmallIntegerField(
        choices=LIKES_DISLIKES_CHOICES,
        verbose_name='like/dislike',
        default=TYPES.LIKE
    )

    updated_at = models.DateTimeField(default=datetime.now)

    class Meta:
        #unique_together = ('user', 'post',)
        db_table = 'posts_like_dislike'
        verbose_name = 'Posts like dislike'
        verbose_name_plural = 'Posts likes dislikes'

    def __str__(self):
        return f'Post - {self.post.title}. User - {self.user.username}'
