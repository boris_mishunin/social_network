from django.test.runner import DiscoverRunner
from rest_framework_simplejwt.tokens import RefreshToken


def get_token(user):
    token = None
    if user:
        refresh = RefreshToken.for_user(user)
        token = str(refresh.access_token)
    return token


class NoDbTestRunner(DiscoverRunner):

    def setup_databases(self):
        pass

    def teardown_databases(self, *args):
        pass
