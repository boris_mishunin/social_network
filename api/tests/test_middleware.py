# coding: utf-8
from django.test import TestCase
from social_network.middleware import RequestMiddleware
from mock import Mock
from .fixtures import UserFactory
from . import get_token
from api.models import Profile


class RequestMiddlewareTestCase(TestCase):
    def setUp(self):
        self.middleware = RequestMiddleware()
        self.request = Mock()
        self.user = UserFactory()
        self.token = get_token(self.user)

    def test_process_requests(self):

        # test bad token

        self.request.META = {}
        self.request.META['HTTP_AUTHORIZATION'] = 'Bearer bad_token'
        self.request.method = 'POST'
        self.request.path = '/test/path'
        self.middleware.process_request(self.request)
        profile = Profile.objects.get(user=self.user)
        self.assertIsNone(profile.last_request_url)

        # test middleware

        self.request.META['HTTP_AUTHORIZATION'] = f'Bearer {self.token}'
        self.middleware.process_request(self.request)
        profile = Profile.objects.get(user=self.user)
        self.assertEqual(profile.last_request_url, self.request.path)
