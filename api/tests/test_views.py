from django.test import TestCase
from .fixtures import PostFactory, UserFactory, PostsLikeDislikeFactory
from rest_framework.test import (
    APIClient
)
from api.models import (
    Post, PostsLikeDislike, Profile
)
from api.serializers import (
    PostSerializer, ProfileSerializer
)
from rest_framework.renderers import JSONRenderer
from json import loads
from django.contrib.auth import get_user_model
import random
from . import get_token


class PostViewSetTestCase(TestCase):
    """

        Test case for post viewset.
    """

    def setUp(self):
        PostFactory.create_batch(5)
        self.posts = Post.objects.all()
        self.user = UserFactory()
        self.token = get_token(self.user)
        self.client = APIClient()
        self.client.credentials(
            HTTP_AUTHORIZATION=f"Bearer {self.token}"
        )
        self.post_data = {
            "title": 'test post',
            "text": 'test text post',
        }

    def test_post_list(self):
        """

        Tests requests of posts list

        :return: None
        """

        response = self.client.get(
            '/api/post/', format='json'
        )
        posts = PostSerializer(
            self.posts, many=True
        ).data

        posts_json_string = JSONRenderer().render(posts)

        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            loads(response.content)['data'],
            loads(posts_json_string)
        )

    def test_post_object(self):
        """

        Tests requests of posts objects

        :return: None
        """

        post = self.posts[0]
        response = self.client.get(
            f'/api/post/{post.id}/', format='json'
        )
        post = PostSerializer(
            post
        ).data

        post_json_string = JSONRenderer().render(post)

        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            loads(response.content)['data'],
            loads(post_json_string)
        )

    def test_post_add(self):
        """

        Tests requests of posts add

        :return: None
        """

        response = self.client.post(
            '/api/post/', data=self.post_data, format='json'
        )
        self.assertEqual(response.status_code, 201)


class PosLikesDislikestViewSetTestCase(TestCase):
    """

        Test case for like dislike viewset.
    """

    def setUp(self):
        PostFactory.create_batch(2)
        UserFactory.create_batch(5)
        self.posts = Post.objects.all()
        self.users = get_user_model().objects.all()
        self.user = self.users[0]
        self.token = get_token(self.user)
        self.client = APIClient()
        self.client.credentials(
            HTTP_AUTHORIZATION=f"Bearer {self.token}"
        )
        self.analitics_data = []
        dates = ['2020-04-21', '2020-04-30']
        for i, post in enumerate(self.posts):
            data = {
                "day": dates[i],
                "likes": 0,
                "dislikes": 0
            }
            for user in self.users:
                like = random.randint(0, 1)
                PostsLikeDislikeFactory(
                    post=post,
                    user=user,
                    like_dislike=like,
                    updated_at=dates[i]
                )

                data['likes'] += like
                data["dislikes"] += 1 if like == 0 else 0
            self.analitics_data.append(data)
        self.likes = PostsLikeDislike.objects.all()

    def test_analitics(self):
        """

        Tests analitics view

        :return: None
        """

        response = self.client.get(
            '/api/analitics/?date_from=2020-04-20&date_to=2020-05-02',
            format='json'
        )
        analitics_json_string = JSONRenderer().render(self.analitics_data)

        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            loads(response.content)['data'],
            loads(analitics_json_string)
        )

        # check no date from

        response = self.client.get(
            '/api/analitics/?date_to=2020-05-02',
            format='json'
        )
        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            loads(response.content)['data'],
            loads(analitics_json_string)
        )

        # check no date to

        response = self.client.get(
            '/api/analitics/?date_from=2020-04-20',
            format='json'
        )
        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            loads(response.content)['data'],
            loads(analitics_json_string)
        )

    def test_like_dislike_add(self):
        """

            Tests like or dislike add

            :return: None
        """

        # check add likes

        post = self.posts[0]
        response = self.client.post(
            f'/api/post_like/{post.id}/', format='json'
        )
        self.assertEqual(response.status_code, 201)

        # check add likes with wrong post id

        response = self.client.post(
            '/api/post_like/123/', format='json'
        )
        self.assertEqual(response.status_code, 400)

        # check add dislikes

        post = self.posts[0]
        response = self.client.post(
            f'/api/post_dislike/{post.id}/', format='json'
        )
        self.assertEqual(response.status_code, 201)


class SignUpViewTestCase(TestCase):
    """

        Test case for sign up view.
    """

    def setUp(self):
        self.client = APIClient()
        self.post_data = {
            "username": 'test_user',
            "password": 'User123456',
        }

    def test_sign_up(self):
        """

            Tests sign up

            :return: None
        """

        response = self.client.post(
            f'/api/signup/', data=self.post_data, format='json'
        )
        self.assertEqual(response.status_code, 201)
        is_user_exists = get_user_model().objects.filter(
            username=self.post_data.get('username')
        ).exists()
        self.assertTrue(is_user_exists)


class ObtainTokenPairViewTestCase(TestCase):
    """

        Test case for obtain pair token viewset.
    """

    def setUp(self):
        self.client = APIClient()
        self.user = UserFactory()
        self.user.set_password(self.user.password)
        self.user.save()
        self.post_data = {
            "username": self.user.username,
            "password": 'User1234567',
        }

    def test_obtain_token_get(self):
        """

            Tests get token

            :return: None
        """

        response = self.client.post(
            f'/api/token/', data=self.post_data, format='json'
        )
        self.assertEqual(response.status_code, 200)
        profile = Profile.objects.get(user=self.user)
        self.assertIsNotNone(profile.last_login_date)


class UserActivityViewSetTestCase(TestCase):
    """

        Test case for user activity viewset.
    """

    def setUp(self):
        self.user = UserFactory()
        self.token = get_token(self.user)
        self.client = APIClient()
        self.client.credentials(
            HTTP_AUTHORIZATION=f"Bearer {self.token}"
        )

    def test_user_activity_list(self):
        """

        Tests requests of user activity list

        :return: None
        """

        response = self.client.get(
            '/api/user_activity/', format='json'
        )

        profiles = Profile.objects.all()

        profiles_data = ProfileSerializer(
            profiles, many=True
        ).data

        profiles_json_string = JSONRenderer().render(profiles_data)

        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            loads(response.content)['data'],
            loads(profiles_json_string)
        )
