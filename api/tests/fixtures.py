import factory
from api.models import (
    Post, PostsLikeDislike, Profile
)
import random
from django.contrib.auth import get_user_model


class ProfileFactory(factory.django.DjangoModelFactory):
    """

        Fixture factory for users profiles.
    """
    user = factory.SubFactory('api.tests.fixtures.UserFactory')

    class Meta:
        model = Profile
        django_get_or_create = ('user',)


class UserFactory(factory.django.DjangoModelFactory):
    """

        Fixture factory for users.
    """

    username = factory.Sequence(lambda n: f'user {n}')
    email = factory.Sequence(lambda n: f'user{n}@email.com')
    password = 'User1234567'
    profile = factory.RelatedFactory(
        ProfileFactory,
        'user'
    )

    class Meta:
        model = get_user_model()


class PostFactory(factory.django.DjangoModelFactory):
    """

        Fixture factory for posts.
    """

    user = factory.SubFactory(UserFactory)
    title = factory.Sequence(lambda n: f'Post title {n}')
    text = factory.Sequence(lambda n: f'Post text {n}')

    class Meta:
        model = Post
        django_get_or_create = ('user',)


class PostsLikeDislikeFactory(factory.django.DjangoModelFactory):
    """

        Fixture factory for posts like dislike.
    """

    user = factory.SubFactory(UserFactory)
    post = factory.SubFactory(PostFactory)
    like_dislike = random.randint(0, 1)

    class Meta:
        model = PostsLikeDislike
        django_get_or_create = ('user', 'post')
