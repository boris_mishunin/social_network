from django.test import TestCase
from api.models import (
    Post, PostsLikeDislike, Profile
)
from .fixtures import (
    PostFactory, PostsLikeDislikeFactory, ProfileFactory
)


class TestModelsStrMixin(object):
    def test_models_string(self):
        self.assertEqual(str(self.model), self.model_str)


class PostTestCase(TestCase, TestModelsStrMixin):
    """

        Test case for post model.
    """

    def setUp(self):
        PostFactory()
        self.model = Post.objects.last()
        self.model_str = self.model.title


class PostsLikeDislikeTestCase(TestCase, TestModelsStrMixin):
    """

        Test case for post like dislike model.
    """

    def setUp(self):
        PostsLikeDislikeFactory()
        self.model = PostsLikeDislike.objects.last()
        self.model_str = (
            f'Post - {self.model.post.title}. User - {self.model.user.username}'
        )


class ProfileTestCase(TestCase, TestModelsStrMixin):
    """

        Test case for profile model.
    """

    def setUp(self):
        ProfileFactory()
        self.model = Profile.objects.last()
        self.model_str = f'Profile of {self.model.user.username}'
