from django.contrib import admin
from .models import PostsLikeDislike, Post, Profile


admin.site.register(Post)
admin.site.register(PostsLikeDislike)
admin.site.register(Profile)
